import type {ReactNode} from 'react';
import {ChatBlock} from "@/layouts/ChatBlock";
import {ProfileBar} from "@/layouts/ProfileBar";
import {ListConvs} from "@/Data/message";
import {SearchBar} from "@/layouts/SearchBar";
import {ListPeoples} from "@/layouts/ListPeoples";
import React, { useEffect, useState} from 'react';
import {useUser} from '@auth0/nextjs-auth0';
import {useRouter} from 'next/router'

type IMainProps = {
    meta: ReactNode;
    children: ReactNode;
};

function Main(props: IMainProps) {
    const [currentConversation, setCurrentConversation] = useState();
    const [flag, setFlag] = useState(false);
    const {user, error, isLoading} = useUser();
    const router = useRouter();

    const [currentUser, setCurrentUser] = useState();

    const options = {
        method: 'GET',
        headers: {}
    };

    useEffect(() => {
            fetch(`http://social.brev.al/msg/api/users?filters[auth0_id][$eq]=${user?.sub}`, options)
                .then(response => response.json())
                .then(response => {
                        if (response.length > 0) {
                            setCurrentUser(response[0]);
                            console.log(response[0]);
                        }
                    }
                )
                .catch(err => console.error(err));
        }, [user]
    );

    if (isLoading) return (
        <div className={"min-h-[100vh] bg-sky-100 grid grid-cols-12 relative"}>
            <div
                className={"absolute top-0 left-0 w-full h-full bg-[url('/assets/images/taches.svg')] bg-cover opacity-5 pointer-events-none z-10"}></div>
            <div className={"col-span-6 z-20 flex justify-center items-center"}>
                <div className={"w-1/2"}>
                    <h1 className={"text-2xl font-bold font-roboto pb-[11px]"}>Loading......</h1>
                </div>
            </div>
            <div className={"col-span-6 z-20 h-full flex justify-center items-center"}>
                <img src="/assets/images/internet.svg" alt="" className={"w-[512px] h-[512px]"}/>
            </div>
        </div>
    );

    if (error) return (
        <div className={"min-h-[100vh] bg-sky-100 grid grid-cols-12 relative"}>
            <div
                className={"absolute top-0 left-0 w-full h-full bg-[url('/assets/images/taches.svg')] bg-cover opacity-5 pointer-events-none z-10"}></div>
            <div className={"col-span-6 z-20 flex justify-center items-center"}>
                <div className={"w-1/2"}>
                    <h1 className={"text-2xl font-bold font-roboto pb-[11px]"}>{error.message}......</h1>
                </div>
            </div>
            <div className={"col-span-6 z-20 h-full flex justify-center items-center"}>
                <img src="/assets/images/internet.svg" alt="" className={"w-[512px] h-[512px]"}/>
            </div>
        </div>);

    if (user) {
        if (currentUser) {
            return (
                <div className="w-full grid grid-cols-12 relative">
                    {props.meta}
                    <div className={"bg-white col-span-4 min-h-[100vh] border-r-2 border-slate-200"}>
                        <div className={"bg-hublot_white-50 h-[10vh] flex items-center px-[50px] justify-between"}>
                            <div className={"flex items-center"}>
                                <img src={user.picture ?? "/assets/images/skyler-ewing-ZajyJH7gSMo-unsplash.jpg"}
                                     alt="pp"
                                     className={"w-[50px] h-[50px] rounded-full"}></img>
                                <h1 className={"font-roboto font-bold text-2xl pl-[30px]"}>Discussion</h1>
                            </div>
                            <button onClick={() => setFlag(!flag)}><img className={"w-[30px] h-[30px]"}
                                                                        src="/assets/images/chat.svg"
                                                                        alt=""/></button>
                        </div>
                        <div className={"overflow-y-scroll max-h-[90vh] flex flex-col pt-[50px] gap-4"}>
                            <ListConvs currentConversation={currentConversation}
                                       setCurrentConversation={setCurrentConversation}/>
                        </div>
                    </div>
                    <div className={"grid grid-cols-12 col-span-8 w-full"}>
                        <div className={"col-span-12 w-full"}>
                            {flag ? <div>
                                <SearchBar/>
                                <ListPeoples/>
                            </div> : <div>
                                {/* @ts-ignore*/}
                                <ProfileBar currentConversation={currentConversation}
                                            setCurrentConversation={setCurrentConversation}></ProfileBar>
                                { /* content here */}
                                <ChatBlock currentConversation={currentConversation}
                                           setCurrentConversation={setCurrentConversation}
                                           currentUser={currentUser}></ChatBlock>
                            </div>}
                        </div>
                    </div>
                    <div className={"absolute top-0 right-0 m-[20px] flex justify-center items-center"}>
                        <a href="/api/auth/logout" className={"hover:border-none"}><img src="/assets/images/log-out.svg"
                                                                                        alt="logout"
                                                                                        className={"h-[50px] w-[50px]"}/></a>
                    </div>
                </div>
            );
        } else {
            return (
                <div className={"min-h-[100vh] bg-sky-100 grid grid-cols-12 relative"}>
                    <div
                        className={"absolute top-0 left-0 w-full h-full bg-[url('/assets/images/taches.svg')] bg-cover opacity-5 pointer-events-none z-10"}></div>
                    <div className={"col-span-6 z-20 flex justify-center items-center"}>
                        <div className={"w-1/2"}>
                            <h1 className={"text-2xl font-bold font-roboto pb-[11px]"}>erreur......</h1>
                        </div>
                    </div>
                    <div className={"col-span-6 z-20 h-full flex justify-center items-center"}>
                        <img src="/assets/images/internet.svg" alt="" className={"w-[512px] h-[512px]"}/>
                    </div>
                </div>
            )
        }
    }
    router.push('/api/auth/login');

    return null
}

export {Main};
