import '../styles/global.css';

import type {AppProps} from 'next/app';
import {UserProvider} from '@auth0/nextjs-auth0';

const MyApp = ({Component, pageProps}: AppProps) => (
    <UserProvider>
        <Component {...pageProps} />
    </UserProvider>
);

export default MyApp;
