import {Meta} from '@/layouts/Meta';
import {Main} from '@/templates/Main';

const Index = () => {
    return (
        <Main
            meta={
                <Meta
                    title="Hublot Social"
                    description="Le réseau social Hublot Social !"
                />
            }
        >
            <h1 className="text-2xl font-bold">Hublot Social</h1>
        </Main>
    );
};

export default Index;
