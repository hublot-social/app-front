import {IPeople} from "@/Interfaces/IPeople";
import {CardPeople} from "@/layouts/CardPeople";
import {IUser} from "@/Interfaces/IUser";

export var users: IUser[] = []

export let peoples: IPeople[] = [
    {
        lastname: "nom 1",
        firstname: "prenom 1",
        src: "/assets/images/skyler-ewing-ZajyJH7gSMo-unsplash.jpg",
    },
    {
        lastname: "nom 2",
        firstname: "prenom 2",
        src: "/assets/images/skyler-ewing-ZajyJH7gSMo-unsplash.jpg",
    },
    {
        lastname: "nom 3",
        firstname: "prenom 3",
        src: "/assets/images/skyler-ewing-ZajyJH7gSMo-unsplash.jpg",
    },
    {
        lastname: "nom 4",
        firstname: "prenom 4",
        src: "/assets/images/skyler-ewing-ZajyJH7gSMo-unsplash.jpg",
    },
    {
        lastname: "nom 1",
        firstname: "prenom 1",
        src: "/assets/images/skyler-ewing-ZajyJH7gSMo-unsplash.jpg",
    },
    {
        lastname: "nom 2",
        firstname: "prenom 2",
        src: "/assets/images/skyler-ewing-ZajyJH7gSMo-unsplash.jpg",
    },
    {
        lastname: "nom 3",
        firstname: "prenom 3",
        src: "/assets/images/skyler-ewing-ZajyJH7gSMo-unsplash.jpg",
    },
    {
        lastname: "nom 4",
        firstname: "prenom 4",
        src: "/assets/images/skyler-ewing-ZajyJH7gSMo-unsplash.jpg",
    },
    {
        lastname: "nom 1",
        firstname: "prenom 1",
        src: "/assets/images/skyler-ewing-ZajyJH7gSMo-unsplash.jpg",
    },
    {
        lastname: "nom 2",
        firstname: "prenom 2",
        src: "/assets/images/skyler-ewing-ZajyJH7gSMo-unsplash.jpg",
    },
    {
        lastname: "nom 3",
        firstname: "prenom 3",
        src: "/assets/images/skyler-ewing-ZajyJH7gSMo-unsplash.jpg",
    },
    {
        lastname: "nom 4",
        firstname: "prenom 4",
        src: "/assets/images/skyler-ewing-ZajyJH7gSMo-unsplash.jpg",
    },
    {
        lastname: "nom 4",
        firstname: "prenom 4",
        src: "/assets/images/skyler-ewing-ZajyJH7gSMo-unsplash.jpg",
    },
    {
        lastname: "nom 4",
        firstname: "prenom 4",
        src: "/assets/images/skyler-ewing-ZajyJH7gSMo-unsplash.jpg",
    },
]


export const listItemsPeoples = peoples.map((people) => <CardPeople firstname={people.firstname}
                                                             lastname={people.lastname} src={people.src}></CardPeople>)
