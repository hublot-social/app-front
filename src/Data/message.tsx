import {CardMessage} from "@/layouts/CardMessage";
// @ts-ignore
import _ from "lodash";
import {useEffect, useState} from "react";

export let message: string = "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad amet asperiores assumenda blanditiis cum" +
    "eaque facilis illum impedit, laborum quasi similique soluta, vero voluptate. Debitis facilis itaque iure" +
    "perferendis totam!" +
    "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad amet asperiores assumenda blanditiis cum" +
    "eaque facilis illum impedit, laborum quasi similique soluta, vero voluptate. Debitis facilis itaque iure" +
    "perferendis totam!"

// var conversations: IConversations[] = [];
const ListConvs = (props: any) => {
        const [conversations, setConversations] = useState([]);

        const options = {
            method: 'GET',
            headers: {}
        };

        useEffect(() => {
                fetch('http://social.brev.al/msg/api/conversations?populate[messages][populate][0]=owner', options)
                    .then(response => response.json())
                    .then(response => {
                            for (const responseElement of response.data) {
                                responseElement.attributes.messages.data = _.sortBy(responseElement.attributes.messages.data, 'attributes.createdAt').reverse();
                            }
                            setConversations(response.data);
                        }
                    )
                    .catch(err => console.error(err));
            }, []
        )
        ;

        if (conversations) {
            return (
                <div>
                    {
                        // @ts-ignore
                        conversations.map((conversation, index) => {
                            // @ts-ignore
                            return (<CardMessage currentConversation={props.currentConversation} setCurrentConversation={props.setCurrentConversation} isSelected={conversation.id == props.currentConversation} id={conversation.id} title={conversation?.attributes.title ?? ""} datelastmessage={conversation?.attributes.messages.data[0].attributes.createdAt} lastmessage={conversation?.attributes.messages.data[0].attributes.content} owner={conversation?.attributes.messages.data[0].attributes.owner.data.attributes}>{conversation.attributes.messages.data[0].attributes.content}</CardMessage>)
                        })
                    }

                </div>
            );
        } else {
            return <p>No conversations</p>;
        }

    }
;


export {ListConvs};

