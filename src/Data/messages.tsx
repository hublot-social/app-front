import {MessageBloc} from "@/layouts/MessageBloc";

import {useEffect, useState} from "react";


const ListMessages = (props: any) => {


    const [messages, setMessages] = useState([]);
    const [isLoading, setLoading] = useState(true);


    // console.log(props.currentConversation);

    // @ts-ignore
    useEffect(() => {
        if (props.currentConversation) {
            getMessage();
            const interval = setInterval(() => {
                getMessage();
            }, 5000);
            return () => clearInterval(interval);
        }
    }, [props.currentConversation])


    const getMessage = async () => {
        const res = await fetch(
            `http://social.brev.al/msg/api/messages?populate=*&filters[conversation_id][id][$eq]=${props.currentConversation}&sort=createdAt:desc`
            , {
                headers: {
                    'Content-Type': 'application/json',
                }
            });

        let data = await res.json();

        setMessages(data.data);
        setLoading(false);
    }


    if (isLoading) {
        return <div className={"h-full w-full flex justify-center items-center opacity-10"}>
            <img src="/assets/images/internet.svg" alt="" className={"w-1/2 object-fill"}/>
        </div>;
    }

    return (
        <>
            {
                messages ?
                    // @ts-ignore
                    messages.map((message) => <MessageBloc message={message.attributes.content} media_id={message.attributes.media_id} date={message.createdAt} owner={message.attributes.owner} currentUser={props.currentUser} messages={messages}/>)
                    :
                    <div className={"h-full w-full flex justify-center items-center"}>
                        <img src="/assets/images/internet.svg" alt="" className={"w-1/2 object-fill"}/>
                    </div>
            }
        </>);


};


export {ListMessages};
