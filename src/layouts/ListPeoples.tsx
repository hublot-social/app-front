import {IUser} from "@/Interfaces/IUser";
import {CardPeople} from "@/layouts/CardPeople";

var users: IUser[] = []

const ListPeoples = () => {
    const callAPI = async () => {
        try {
            const res = await fetch(
                `http://social.brev.al/msg/api/users`
            , {
                headers: {
                    'Content-Type': 'application/json',
                }
                });
            users = await res.json();
        } catch (err) {
            console.log(err);
        }
    };

    callAPI();


    return (
        <div className={"text-white h-[90vh] flex w-full overflow-y-scroll grid grid-cols-12 gap-x-20 gap-y-5 p-10"}>
            { /* content here */}
            {
                users.map((IUser) => <CardPeople firstname={IUser.first_name}
                                                 lastname={IUser.last_name}
                                                 src={"/assets/images/skyler-ewing-ZajyJH7gSMo-unsplash.jpg"}></CardPeople>)
            };
            {/*{listItemsPeoples}*/}
        </div>
    );
};

export {ListPeoples};
