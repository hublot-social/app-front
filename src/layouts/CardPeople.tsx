const CardPeople = (props: any) => {
    return (
        <div className={"col-span-3 p-10 bg-hublot_white-50 shadow-2xl border-t-[20px] border-hublot_blue-700"}>
            <div className={"flex justify-center items-center h-full w-full flex-col gap-6"}>
                <img src={props.src ?? "/assets/images/skyler-ewing-ZajyJH7gSMo-unsplash.jpg"}
                     alt={"profile picture " + props.firstname + " " + props.lastname}
                     className={"h-[75px] w-[75px] rounded-full object-fill"}/>
                <h2 className={"font-sans text-hublot_blue-700 font-bold"}>{props.firstname} {props.lastname}</h2>
            </div>
        </div>
    );
};

export {CardPeople};
