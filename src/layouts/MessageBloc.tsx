import {useEffect, useState} from "react";

const MessageBloc = (props: any) => {
    const [isMe, setIsMe] = useState(false);

    useEffect(() => {
        (props.currentUser.id == props.owner.data.id) ? setIsMe(true) : setIsMe(false);
    },[props.messages]);

    // console.log(isMe)
    // console.log(props)
    // console.log(props.currentUser.id)
    console.log(props)
    console.log(isMe)
    return (

        <div>

            <div
                className={((isMe) ? "flex flex-row-reverse justify-start items-end py-4" : "flex justify-start items-end py-4")}>
                <img src={props.owner.data.attributes.image} alt={"pp"}
                     className={"h-[50px] w-[50px] min-w-[50px] rounded-full object-cover"}/>
                <div
                    className={(isMe ? "flex flex-wrap px-5 justify-start" : "flex flex-wrap px-5 justify-end h-auto")}>
                    {
                        props.media_id ? <div
                                className={isMe ? "w-full flex justify-end mb-[10px]" : "w-full flex justify-start mb-[10px]"}>
                                <img src={"http://social.brev.al/media" + props.media_id} alt={"pp"}
                                     className={"h-[350px] min-w-[300px] max-w-[50%] rounded-2xl object-cover"}/>
                            </div>
                            : null
                    }
                    {props.message != "" &&
                        <div className={isMe ? "h-auto w-full flex justify-end" : "h-auto w-full flex justify-start"}>
                            <p className={(isMe ? "text-slate-50 bg-hublot_blue-300" : "text-slate-800 bg-slate-300") + " rounded-2xl text-sm min- w-[800px] p-5 rounded-xl"}>
                                {props.message}
                            </p>
                        </div>
                    }
                </div>
            </div>
        </div>
    );
};

export {MessageBloc};
