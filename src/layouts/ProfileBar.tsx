import { useUser } from '@auth0/nextjs-auth0';

const ProfileBar = () => {
    const { user, error, isLoading } = useUser();

    if (isLoading) return <div>Loading...</div>;
    if (error) return <div>{error.message}</div>;

    if (user) {
        return (
            <div className={"bg-hublot_white-50 border-b-2 border-slate-200 flex items-center w-full h-[10vh]"}>
                <div className={"flex px-[30px]"}>
                    <img src={user.picture ?? "/assets/images/skyler-ewing-ZajyJH7gSMo-unsplash.jpg"}
                         alt={"profile picture " + user.first_name + " " + user.last_name}
                         className={"h-[50px] w-[50px] rounded-full object-fill"}/>
                    <input type="text"
                           className={"w-[60vw] font-semibold text-slate-700 opacity-100 focus:border-none focus:outline-none " +
                               "text-lg bg-transparent border-none px-[30px]"}
                           value={`${user.nickname}`} readOnly={true}/>
                </div>
            </div>
        );
    }
    return <div>No user</div>;
};

export {ProfileBar};
