import {SendMessage} from "@/layouts/SendMessage";
import {ListMessages} from "@/Data/messages";

const ChatBlock = (props: any) => {
    // const [childCurrentConversation] = useState();

    // console.log(props.currentConversation);
    // useEffect(() => {
    //     props.wrapperSetCurrentConversation(childCurrentConversation);
    // }, [props.currentConversation, childCurrentConversation]);


    return (
        <div className={"text-white h-[90vh] max-h-[90vh] flex w-full grid grid-cols-12"}>
            <div className={"h-full w-full max-h-[90vh] flex-col flex col-span-full"}>
                <div className={"h-[90%] w-full overflow-y-scroll flex flex-col-reverse px-10"}>



                    {/*@ts-ignore*/}
                    <ListMessages  currentConversation={props.currentConversation} currentUser={props.currentUser}/>


                </div>
                <div className={"h-[10%] w-full"}>
                    <SendMessage   currentConversation={props.currentConversation} currentUser={props.currentUser} />
                </div>
            </div>
        </div>
    );
};

export {ChatBlock};
