import {useEffect, useState} from "react";

const SendMessage = (props: any) => {


    const [message, setMessage] = useState("");
    const [imageLink, setImageLink] = useState(null);

    const sendMessage = async () => {
        try {


            if (message != "" || imageLink != null) {

                const res = await fetch(
                    `http://social.brev.al/msg/api/messages`
                    , {
                        method: "POST",
                        headers: {
                            'Content-Type': 'application/json',
                        },
                        body: imageLink ? `{\"data\":{\"media_id\":\"${imageLink}\",\"content\": \"${message}\",\"conversation_id\":\"${props.currentConversation}\",\"owner\":\"${props.currentUser.id}\"}}` :
                            `{\"data\":{\"media_id\": null,\"content\": \"${message}\",\"conversation_id\":\"${props.currentConversation}\",\"owner\":\"${props.currentUser.id}\"}}`

                    });
                let data = await res.json();
                console.log(data);


                useEffect(() => {
                    setMessage("");
                    setImageLink(null);
                });

            }

        } catch (err) {
            console.log(err);
        }
    }


    const uploadImage = async (change: React.ChangeEvent<HTMLInputElement>) => {
        try {

            let dataform = new FormData()
            // @ts-ignore
            dataform.append('files', change.target.files[0])


            const res = await fetch(
                `http://social.brev.al/media/api/upload`,
                {
                    method: "POST",
                    // @ts-ignore
                    body: dataform
                });
            let data = await res.json();
            setImageLink(data[0].url);
        } catch (err) {
            console.log(err);
        }
    };

    const checkKey = (e: any) => {
        if (e.key === "Enter") {
            sendMessage();
        }
    }

    return (
        <div className={"w-full h-full flex justify-center"}>
            <div className={"w-1/12 flex items-center justify-end pr-6"}>
                <input className={"hidden"} type="file" name="files" id={"fileinput"} onChange={(change) => {
                    uploadImage(change)
                }}/>
                <div>
                    <img src="/assets/images/document.svg" alt="piece jointe"
                         className={"w-[40px] h-[40px] cursor-pointer"} onClick={() => {
                        document.getElementById("fileinput")?.click()
                    }}/>
                </div>
            </div>
            <div className={"w-10/12 flex items-center"}>
                <input type={"text"} value={message}
                       className={"w-full bg-slate-100 p-3 pl-6 text-slate-700 text-sm rounded focus:border-none focus:outline-none h-[40px]"}
                       placeholder={"Aa"} onKeyUp={(e) => {
                    checkKey(e)
                }} onChange={(change) => {
                    setMessage(change.target.value);
                }}/>
            </div>
            <div className={"w-1/12 flex items-center pl-4"}>
                <button
                    onClick={() => {
                        sendMessage();
                    }}
                ><img src="/assets/images/send.svg" alt="piece jointe" className={"w-[40px] h-[40px]"}/></button>
            </div>
        </div>
    );
};

export {SendMessage};