const SearchBar = () => {
    return (
        <div className={"bg-hublot_white-50 border-b-2 border-slate-200 flex items-center w-full h-[10vh]"}>
            <div>
                <input type="text"
                       className={"w-[60vw] font-bold text-slate-900 opacity-100 focus:border-none focus:outline-none " +
                           "text-xl bg-transparent border-none px-[30px]"}
                       placeholder={"À :"}/>
            </div>
        </div>
    );
};

export {SearchBar};
