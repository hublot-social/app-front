export type IParticipant = {
    data: {
        id: string,
        attributes: {
            participant_id: string,
            conversation_id: string,
        }
    }
}
