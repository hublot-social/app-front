import {IMsg} from "@/Interfaces/IMsg";

export type ICardMessage = {
    isSelected: boolean,
    title: string,
    lastmessage?: IMsg,
    datelastmessage?: string
}
