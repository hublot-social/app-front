export type IMessage = {
    from: string,
    to: string,
    media_id: string | null,
    message: string,
    date: string,

}
