export type IMsg = {
    media_id: string,
    conversation_id: string,
    date: string,
    content: string,
}
