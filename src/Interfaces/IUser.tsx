export type IUser = {
    id: number,
    username: string,
    email: string,
    provider: string,
    confirmed: boolean,
    blocked: boolean,
    created_at: string,
    updated_at: string,
    first_name: string,
    last_name: string,
    auth0_id: string,
}
