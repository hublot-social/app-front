export type IPeople = {
    firstname: string,
    lastname: string,
    src: string
};
