import {IParticipant} from "@/Interfaces/IParticipant";
import {IMsg} from "@/Interfaces/IMsg";

export type IConversations = {
    id: string,
    title: string,
    attributes: {
        title: string,
        participants: IParticipant[];
        messages: {
            data : IMsg[]
        };
    }
}
