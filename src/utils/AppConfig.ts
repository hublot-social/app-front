export const AppConfig = {
  site_name: 'Web App Hublot Social',
  title: 'Hublot Social',
  description: 'Le réseau social d\'Hublot !',
  locale: 'fr',
};
