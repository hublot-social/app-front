# run the project

````
npm install
````

- create .env.local file in root directory
- follow the .env.local.exemple

````
npm run dev
#or 
npm run build + npm run start
````

### How it works

-> when we start the app, we are redirected to auth0 -> if we are logged, the app is accessible


### API 
````
/media
/msg
/user
````
