# Install dependencies only when needed
FROM node:lts-alpine AS deps

WORKDIR /opt/app
COPY package.json yarn.lock ./
RUN yarn install --frozen-lockfile

# Rebuild the source code only when needed
# This is where because may be the case that you would try
# to build the app based on some `X_TAG` in my case (Git commit hash)
# but the code hasn't changed.
FROM node:lts-alpine AS builder

ENV NODE_ENV=production
WORKDIR /opt/app
COPY . .
COPY --from=deps /opt/app/node_modules ./node_modules
RUN yarn build

# Production image, copy all the files and run next
FROM node:lts-alpine AS runner

ARG X_TAG
WORKDIR /opt/app
ENV NODE_ENV=production



# A long, secret value used to encrypt the session cookie
ENV AUTH0_SECRET='0f55f69b6efd18fe84b3a0d8cdee5e7b859af9d97c1eb7f8e477bcf0ce4826ba'
# The base url of your application
ENV AUTH0_BASE_URL='http://localhost:3000'
# The url of your Auth0 tenant domain
ENV AUTH0_ISSUER_BASE_URL='https://dev-pdslcs6c.us.auth0.com'
# Your Auth0 application's Client ID
ENV AUTH0_CLIENT_ID='ptAmgFtJrt5RNhQH19neGRqca68bPt2j'
# Your Auth0 application's Client Secret
ENV AUTH0_CLIENT_SECRET='LdXO1RCahNK0k7m_l0Ebuf22_iCTHwINNTlQrQpnmED5ZkdE2U09abW0QZQ7UcS2'



COPY --from=builder /opt/app/next.config.js ./
COPY --from=builder /opt/app/public ./public
COPY --from=builder /opt/app/.next ./.next
COPY --from=builder /opt/app/node_modules ./node_modules
CMD ["node_modules/.bin/next", "start"]